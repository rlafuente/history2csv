
A dead simple script to read a zsh history file and output a CSV.

It goes from this

    : 1438000522:0;dmesg | tail
    : 1438000529:0;lsusb
    : 1438000603:0;sudo killall adb

to this:

    2015-07-27T14:35:22,dmesg | tail
    2015-07-27T14:35:29,lsusb
    2015-07-27T14:36:43,sudo killall adb
