#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys


def timestamp2iso8601(d):
    import datetime
    return datetime.datetime.fromtimestamp(int(d)).strftime('%Y-%m-%dT%H:%M:%S')


f = sys.argv[1]
lines = open(f, 'r').readlines()
for line in lines:
    line = line.strip()
    if len(line) < 3:
        continue
    try:
        time = line.split(":")[1]
        cmd = line.split(";")[1]
    except:
        print line
    print timestamp2iso8601(time) + "," + cmd
